const { application } = require("express");
const express = require("express");
const cors = require('cors');
const app = express();
const port = 8000;
const db = require("./db-config");
const multer  = require('multer')
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Authenticate = require("./middleware/Authenticate");
const UserAuth = require("./middleware/UserAuth");
var cookieParser = require('cookie-parser')
const StreamZip = require('node-stream-zip');
var path = require('path');
const decompress = require("decompress");
const { async } = require("node-stream-zip");
var randtoken = require('rand-token');
var nodemailer = require('nodemailer');


const baseurl = "http://54.95.193.11:3000/";


// const upload = multer({ dest: 'uploads/' })
app.use(cors());
app.use(express.json());
app.use(cookieParser());

app.use("/uploads", express.static("uploads"));

app.get("/icons", Authenticate, (req,res)=>{
    
    db.query('select users.*, icons.* from users,icons where icons.user_id = users.user_id order by icons.user_id desc', async(Err, result) => {
            // console.log(result);return
            if (Err) {
                res.json({status: false,msg:"There was an error while fetching data. Try again later",error : req.error});
            } else {
                res.json({status: true,msg:"got all icons",icons : result,error : req.error, baseurl});
            }
        });
})

app.get("/getallicons", (req,res)=>{

    console.log(req.error);
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET,POST,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Methods','Content-Type','Authorization');

    db.query('select * from icons', async(Err, result) => {
            if (Err) {
                res.json({status: false,msg:"There was an error while fetching data. Try again later",error : req.error});
            } else {
                res.json({status: true,msg:"got all icons",icons : result,error : req.error, baseurl});
            }
        });
})

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + '-' + file.originalname);
        //   cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
})

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if(req.body.uploadType == 1){
            if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
                cb(null, true);
                req.fileError = 'null';
            } else {
                cb(null, false);
                req.fileError = 'File format is not valid';
            }
        }else{
            if (file.mimetype === 'application/zip') {
                    // var root = path.dirname(file.originalname)s
                cb(null, true);
                req.fileError = 'null';
            } else {
                cb(null, false);
                req.fileError = 'File format is not valid';
            }
        }
        
    }
});

app.post("/icons", upload.single('image'), Authenticate, (req,res)=>{

    const uploaded_by = req.user.user_id;

    if(req.fileError == "null" && req.body.uploadType == 1){
        const icon_image =  req.file.filename;

        // console.log(req.body);return;

        const {name, style, type, tags} = req.body;

        if(name == "" || icon_image == "" || style == "" || type == "" || tags == ""){
            res.json({msg:"please enter the required details"});
        }else{
            const data = {
                "icon_name": name,
                "icon_image": "uploads/"+icon_image,
                "icon_style": style,
                "icon_type": type,
                "icon_tags": tags,
                "user_id" : uploaded_by
            };
            db.query('INSERT INTO icons SET ?', data, async(Err, result) => {
                if (Err) {
                    res.json({status: false,msg:"There was an error while inserting. Try again later"});
                } else {
                    res.json({status: true,msg:"icon inserted successfully"});
                }
            });
        }
    }else if(req.fileError == "null" && req.body.uploadType == 2){

        const {style, type} = req.body;

        let isStatus =  false;

        const zip = new StreamZip({
            file: __dirname + "/uploads/"+req.file.filename,
            storeEntries: true
        });

        zip.on('ready', () => {

            for (const entry of Object.values(zip.entries())) {
                var filepath = entry.name;
                var filename = filepath.substring(filepath.lastIndexOf('/')+1);
                
                var filename_without_ext = path.parse(filename).name;
                
                var icon_name = filename_without_ext;
                var icon_path = filepath;
                var icon_tags = filename_without_ext;

                if(filename != ""){
                    let data = {
                        "icon_name": icon_name,
                        "icon_image": "uploads/"+icon_path,
                        "icon_style": style,
                        "icon_type": type,
                        "icon_tags": icon_tags,
                        "user_id" : uploaded_by
                    };
                    db.query('INSERT INTO icons SET ?', data, async(Err, result) => {
                        if (Err) {
                            isStatus  = true;
                        }else{
                            isStatus  = false;
                        }
                    });
                }
            }
        });
        
        decompress(__dirname + "/uploads/"+req.file.filename, "uploads")
        .then((files) => {
            isStatus = false;
        })
        .catch((error) => {
            isStatus = true;
            console.log(error);
        });

        if(isStatus){
            res.json({status: false,msg:"There was an error while inserting. Try again later"});
        }else{
            res.json({status: true,msg:"icon inserted successfully"});
        }

    }else{
        console.log(req.fileError);
    }

})

app.post("/delete-icons/:icon_id", (req,res)=>{
    const icon_id = req.params.icon_id;
    db.query(`delete from icons where icon_id = ${icon_id}`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while deleting. Please Try again later"});
        }else{
            res.json({status: true,msg:"Icon deleted successfully"});
        }
    })
})

app.post("/edit-icons/:icon_id", (req,res)=>{
    const icon_id = req.params.icon_id;
    db.query(`select * from icons where icon_id = ${icon_id}`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            res.json({status: true,msg:"Icon found", icon : result});
        }
    })
})

app.post("/search-icons", (req,res)=>{
    const {search_val} = req.body;
    db.query(`select icons.*, users.* from icons, users where icons.icon_name like '%${search_val}%' and icons.user_id = users.user_id`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            res.json({status: true,msg:"Icon found", icons : result});
        }
    })
})

app.post("/icons-style", (req,res)=>{
    const {search_val} = req.body;
    if(search_val == "all"){
        db.query(`select * from icons`, async(err,result)=>{
            if(err){
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Icon found", icons : result});
            }
        })
    }else{
        db.query(`select * from icons where icon_style = '${search_val}'`, async(err,result)=>{
            if(err){
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Icon found", icons : result});
            }
        })
    }
})

app.post("/search-filter", (req,res)=>{
    const {searchInput,activeFilter} = req.body;
    if(activeFilter == "all"){
        db.query(`select * from icons where icon_name like '%${searchInput}%'`, async(err,result)=>{
            if(err){
                console.log(1)
                console.log(err)
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Icon found", icons : result});
            }
        })
    }else{
        db.query(`select * from icons where icon_style = '${activeFilter}' and  icon_name like '%${searchInput}%'`, async(err,result)=>{
            if(err){
                console.log(2)
                console.log(err)
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Icon found", icons : result});
            }
        })
    }
})

const editstorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + '-' + file.originalname);
    }
})

const editImage = multer({
    storage: editstorage,
    fileFilter: (req, file, cb) => {
        if(req.files != undefined || file != undefined){
            console.log("if");
            if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
                cb(null, true);
                req.fileError = 'null';
            } else {
                cb(null, false);
                req.fileError = 'File format is not valid';
            }
        }else{
            console.log("else");
            req.file.filename = "null";
        }
    }
});

app.post("/update-icons", editImage.single('image'),(req,res)=>{

    // console.log(req.file);return;

    var datetime = (new Date ((new Date((new Date(new Date())).toISOString() )).getTime() - ((new Date()).getTimezoneOffset()*60000))).toISOString().slice(0, 19).replace('T', ' ');
    // console.log(datetime);return

    const {icon_id,icon_name, icon_style, icon_type, icon_tags} = req.body;
    let info = null;
    
    if( req.file == undefined ||req.file.filename == undefined || req.file.filename == null){
        info = {
            "icon_name": icon_name,
            "icon_style": icon_style,
            "icon_type": icon_type,
            "icon_tags": icon_tags,
            "updated_on": datetime,
        }
    }else{
        info = {
            "icon_name": icon_name,
            "icon_image": "uploads/"+req.file.filename,
            "icon_style": icon_style,
            "icon_type": icon_type,
            "icon_tags": icon_tags,
            "updated_on": datetime,
        }
    }
    db.query('update icons SET ? where ? ', [info, { "icon_id": icon_id }], async(err, result) => {
        if (err) {
            res.json({status: false,msg:"There was an error while updating icon. Please Try again later"});
        } else {
            res.json({status: true,msg:"Icon updated successfully"});
        }
    })
})

app.get("/users", Authenticate, (req,res)=>{
    db.query('select * from users where role_id="3"', async(Err, result) => {
            if (Err) {
                res.json({status: false,msg:"There was an error while fetching data. Try again later", error : req.error});
            } else {
                res.json({status: true,msg:"got all users",users : result, user: req.user, error : req.error});
            }
        });
})

app.post("/users", Authenticate ,async (req,res)=>{
// console.log(req.user);return;

const created_by = req.user.name;
    const {name, email, password} = req.body;

    if(name == "" || email == "" || password == ""){
        res.json({msg:"please enter the required details"});
    }else{

        db.query(`select * from users where email = '${email}'`, async(Err, result) => {
            if (Err) {
                console.log(Err);
                res.json({status: false,msg:"There was an error. Try again later"});
            } else {
                if(result.length > 0){
                    res.json({status: false,msg:"User with this Email Already Exists"});
                }else{
                    let hashpassword = await bcrypt.hash(password, 8);
                    const data = {
                        "name": name,
                        "email": email,
                        "password": hashpassword,
                        "role_id": '3',
                        "user_created_by" : created_by
                    };
                    db.query('INSERT INTO users SET ?', data, async(Err, result) => {
                        if (Err) {
                            res.json({status: false,msg:"There was an error while inserting. Try again later"});
                        } else {
                            res.json({status: true,msg:"User Inserted Successfully"});
                        }
                    });
                }
            }
        });
    }
})

app.post("/delete-users/:user_id", (req,res)=>{
    const user_id = req.params.user_id;
    db.query(`delete from users where user_id = ${user_id}`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while deleting. Please Try again later"});
        }else{
            res.json({status: true,msg:"Users deleted successfully"});
        }
    })
})

app.post("/edit-users/:user_id", (req,res)=>{
    const user_id = req.params.user_id;
    db.query(`select * from users where user_id = ${user_id}`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while deleting. Please Try again later"});
        }else{
            res.json({status: true,msg:"Users deleted successfully", users: result});
        }
    })
})

app.post("/update-users", async (req,res)=>{
    const {user_id,name, email, password} = req.body;
    var info = "";
    if(password != "" && password != undefined && password != null){
        let hashpassword = await bcrypt.hash(password, 8);
        info = {
            "name": name,
            "email": email,
            "password": hashpassword
        }
    }else{
        info = {
            "name": name,
            "email": email
        }
    }
    db.query('update users SET ? where ? ', [info, { "user_id": user_id }], async(err, result) => {
        if (err) {
            res.json({status: false,msg:"There was an error while updating. Please Try again later"});
        } else {
            res.json({status: true,msg:"User updated successfully"});
        }
    })
})

app.post("/search-users", Authenticate, (req,res)=>{
    const {search_val} = req.body;
    // if (res.user != undefined) {
        db.query(`select * from users where name like '%${search_val}%' and role_id='3'`, async(err,result)=>{
            if(err){
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Users found", users : result});
            }
        })
    // }else {
        // res.clearCookie('userRegistered');
        // res.redirect('/');
    // }
})

app.post("/login", async(req,res)=>{
    const {email, password} = req.body;
    db.query(`select * from users where email = '${email}' and role_id!='3'`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            if(result.length > 0){
                var resp = await bcrypt.compare(password, result[0].password);
                if(resp){
                    const regtoken = jwt.sign({ id: result[0].user_id }, "943h9DH(H#R(*#HD(HD(RTH#(*Dh9th9gn498cNA(RN97BR()))))))d@ERR#R%", {
                            expiresIn: "90d",
                            // httpOnly: true
                        })
                        const cookiesOptions = {
                            expiresIn: new Date(Date.now() + "" * 24 * 60 * 60 * 1000),
                            // httpOnly: true
                        }
                        res.cookie('userRegistered', regtoken, cookiesOptions);
                    res.json({status: true,msg:"Login successfully"});
                }else{
                    res.json({status: false,msg:"Invalid login credentials"});
                }
            }else{
                res.json({status: false,msg:"Access Denied"});
            }
        }
    })
})

app.post("/user-login", async(req,res)=>{
    res.header("Access-Control-Allow-Origin", "*");
    const {email, password} = req.body;
    db.query(`select * from users where email = '${email}' and role_id='3'`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            // console.log(result);
            if(result.length > 0){
                var resp = await bcrypt.compare(password, result[0].password);
                if(resp){
                    const regtoken = jwt.sign({ id: result[0].user_id }, "943h9DH(H#R(*#HD(HD(RTH#(*Dh9th9gn498cNA(RN97BR()))))))d@ERR#R%", {
                            expiresIn: "90d",
                            // httpOnly: true
                        })
                        const cookiesOptions = {
                            expiresIn: new Date(Date.now() + "" * 24 * 60 * 60 * 1000),
                            // httpOnly: true
                        }
                        console.log(req.cookies);

                    res.json({status: true,msg:"Login successfully"});
                }else{
                    res.json({status: false,msg:"Invalid login credentials"});
                }
            }else{
                res.json({status: false,msg:"Access Denied"});
            }
        }
    })
})

app.post("/logout", async(req,res)=>{
    res.clearCookie("userRegistered", { path: "/"});
    res.json({status: true,msg:"Logout successfully"})
})

app.get("/admin-users", Authenticate, (req,res)=>{
    db.query('select * from users where role_id = "2"', async(Err, result) => {
            if (Err) {
                res.json({status: false,msg:"There was an error while fetching data. Try again later", error : req.error});
            } else {
                res.json({status: true,msg:"got all users",users : result, user: req.user, error : req.error});
            }
        });
})

app.post("/admin-users", async (req,res)=>{
// console.log(req.body);return;
    const {name, email, password} = req.body;

    if(name == "" || email == "" || password == ""){
        res.json({msg:"please enter the required details"});
    }else{

        db.query(`select * from users where email = '${email}' and role_id = "2"`, async(Err, result) => {
            if (Err) {
                console.log(Err);
                res.json({status: false,msg:"There was an error. Try again later"});
            } else {
                if(result.length > 0){
                    res.json({status: false,msg:"Admin with this Email Already Exists"});
                }else{
                    let hashpassword = await bcrypt.hash(password, 8);
                    const data = {
                        "name": name,
                        "email": email,
                        "password": hashpassword,
                        "role_id": '2',
                    };
                    db.query('INSERT INTO users SET ?', data, async(Err, result) => {
                        if (Err) {
                            res.json({status: false,msg:"There was an error while inserting. Try again later"});
                        } else {
                            res.json({status: true,msg:"User Inserted Successfully"});
                        }
                    });
                }
            }
        });
    }
})

app.post("/search-admin", Authenticate, (req,res)=>{
    const {search_val} = req.body;
    // if (res.user != undefined) {
        db.query(`select * from users where name like '%${search_val}%' and role_id='2'`, async(err,result)=>{
            if(err){
                res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
            }else{
                res.json({status: true,msg:"Users found", users : result});
            }
        })
    // }else {
        // res.clearCookie('userRegistered');
        // res.redirect('/');
    // }
})

app.post("/forgot-password", Authenticate, (req,res)=>{
    const {email} = req.body;
    db.query(`select * from users where email = '${email}' and role_id='3'`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            if(result.length > 0){
                const secret = randtoken.generate(12);
                let hashpassword = await bcrypt.hash(secret, 8);
                const payload = {
                    email: result[0].email,
                    id: result[0].id
                }
                // const token = jwt.sign(payload,secret,{expiresIn:'5m'});
                var sent = sendEmail(email, secret);
                if (sent != '0') {
                    db.query(`update users set password = '${hashpassword}' where email='${email}' and role_id='3'`, async(err,result)=>{
                        if(err){
                            res.json({status: true,msg:"Email sent but there was an error. Please try again later", users : result});
                        }else{
                            res.json({status: true,msg:"Email sent", users : result});
                        }
                    })
                }else{
                    res.json({status: false,msg:"Email failed", users : result});
                }
            }
        }
    })
})

app.post("/change-password", Authenticate, (req,res)=>{
    const {email, oldPassword, newPassword} = req.body;
    db.query(`select * from users where email = '${email}' and role_id='3'`, async(err,result)=>{
        if(err){
            res.json({status: false,msg:"There was an error while fetching data. Please Try again later"});
        }else{
            if(result.length > 0){
                var resp = await bcrypt.compare(oldPassword, result[0].password);
                if(resp){
                    let hashpassword = await bcrypt.hash(newPassword, 8);
                    db.query(`update users set password = '${hashpassword}' where email='${email}' and role_id='3'`, async(err,result)=>{
                        if(err){
                            res.json({status: false,msg:"Change Password Failed", users : result});
                        }else{
                            res.json({status: true,msg:"Password Updated", users : result});
                        }
                    })
                }else{
                    res.json({status: false,msg:"Invalid Credentials"});
                }
            }
        }
    })
})

function sendEmail(email, token) {
    var email = email;
    var token = token;
    var mail = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
            user: 'growwithkvell@gmail.com', // Your email id
            pass: 'jwlgwrgjtustlxhu' // Your password
        }
    });
    var mailOptions = {
        from: mail.options.auth.user,
        to: email,
        subject: 'Huge icon - Temporary Password',
        html   : '<p>You requested for reset password, Your new temporary password is: ' + token + '. You can use this password as one time login and then update the password from the top right menu after login in the Huge icon plugin in figma</p>'
    }
    mail.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log('1');
            console.log(error);
        } else {
            console.log(0)
        }
    });
}

app.listen(port, ()=>{
    console.log(`app is running at port no ${port}`);
})